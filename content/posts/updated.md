+++
title = "Transfer this Website From GitLab Pages to AWS S3"
date = "2024-03-04"
+++

# This is the Project 1 of DUKE IDS 721 -- Updated Personal Webpage of Yang Ouyang


## Project Overview
This individual project is part of the Duke University IDS 721 course, focusing on static webpage deployment skills on AWS S3. Visit the webpage to explore my work, insights, and projects developed throughout the course.

[**Visit My New Webpage**](http://oyypersonalwebsite.s3-website-us-east-1.amazonaws.com)

## A little update on the CI/CD pipeline
```yaml
stages:
  - build
  - deploy

image: alpine:latest

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  S3_BUCKET_NAME: 
  AWS_DEFAULT_REGION: us-east-1
  # AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY should be set in GitLab CI/CD variables for security

build:
  stage: build
  script:
    - apk add --update-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ zola
    - zola build
  artifacts:
    paths:
      - public
  only:
    - main

deploy:
  stage: deploy
  script:
    - apk add --no-cache python3 py3-pip python3-dev build-base libffi-dev
    - python3 -m venv awscli-venv
    - source awscli-venv/bin/activate
    - pip install awscli
    - aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
    - aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
    - aws configure set default.region $AWS_DEFAULT_REGION
    - aws s3 sync public s3://$S3_BUCKET_NAME/ --delete
    - deactivate
  dependencies:
    - build
  only:
    - main
```