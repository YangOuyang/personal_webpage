+++
title = "Welcome to the World of IDS 721 Cloud Computing"
date = "2024-01-19"
+++

# This is the Mini Project 1 of DUKE IDS 721 -- Personal Webpage of Yang Ouyang
Welcome to the repository for the Personal Webpage of Yang Ouyang, a showcase project for Duke IDS 721. This project demonstrates web development skills, hosting on GitLab Pages, and features a personal portfolio including blog posts and projects.

## Project Overview
This mini project is part of the Duke University IDS 721 course, focusing on practical web development and deployment skills. Visit the webpage to explore my work, insights, and projects developed throughout the course.

[**Visit My Webpage**](https://yangouyang.gitlab.io/personal_webpage)
[**Visit My Webpage on AWS S3**](http://oyypersonalwebsite.s3-website-us-east-1.amazonaws.com)

## Installation and Setup on Mac
Follow these steps to set up your Zola environment on a Mac, ensuring a smooth development process for your personal webpage project.

1. **Install Zola**: First, install Zola using Homebrew by running `brew install zola` in your terminal. This command installs the latest version of Zola, the static site generator used for this project.

2. **Project Initialization**: After installation, navigate to your project directory in the terminal and run `zola init my_website` to initialize a new Zola project. Replace `my_website` with your project name. This command creates the necessary project structure and configuration files.

3. **Running the Zola Server**: To view your site locally as you develop, run `zola serve` within your project directory. This command starts a local server, usually accessible at `http://127.0.0.1:1111`, allowing you to preview your site in real-time.

By following these steps, you'll have Zola set up on your Mac, ready for website development and content creation.

### Theme Customization
To personalize webpage and ensure it aligns with your unique style and functional requirements, follow these steps for theme customization using Zola:

1. **Selecting a Theme**: Browse the [Zola themes directory](https://www.getzola.org/themes/) to find a theme that suits your website's aesthetics and functionality needs. Each theme comes with its own set of features and design elements.

2. **Installing the Theme**: Once you've selected a theme, clone its repository into your `themes` directory within your Zola project. For example, `git clone [theme-git-url] themes/[theme-name]`.

3. **Configuring the Theme**: Edit your `config.toml` file to set the `theme` variable to the name of your new theme. This file is also where you can customize various aspects of the theme, such as color schemes, font styles, and layout options, depending on the theme's available customization options.

4. **Customizing Theme Templates**: For more in-depth customization, you can modify the theme's HTML templates directly. These are located in the `templates` directory within the theme's folder. Be cautious with these changes, as they can affect the core structure of your site.

By carefully selecting and customizing a theme, you can create a webpage that not only looks professional but also reflects your personal brand and meets your specific needs.

## Website Structure
An overview of the website's architecture, including navigation between the About Me, Posts, and Projects pages, facilitating easy access to all sections.

### Featured Sections
- **About Me**: An introduction of myself.
- ### Visual Previews

- **Posts Page**: A collection of insightful blog posts and reflections.
  ![Posts Page Preview](image.png)
- **Projects Page**: A portfolio showcasing the projects undertaken during the IDS-721 course or DUKE ECE.
  ![Projects Page Preview](image-1.png)
## Deployment to GitLab Pages
Deploying your Zola site to GitLab Pages requires setting up a CI/CD pipeline. This ensures that your site automatically updates every time you push changes to your repository.

1. **Create `.gitlab-ci.yml`**: In your project's root, add a `.gitlab-ci.yml` file. This YAML file will define the CI/CD pipeline steps GitLab uses to build and deploy your site.

2. **Configure the Pipeline**:
```yaml
image: alpine:latest
variables:
# This variable will ensure that the CI runner pulls in your theme from the submodule
GIT_SUBMODULE_STRATEGY: recursive

pages:
script:
# Install the zola package from the alpine community repositories
- apk add --update-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ zola
# Execute zola build
- zola build

artifacts:
paths:
# Path of our artifacts
- public

# This config will only publish changes that are pushed on the main branch
only:
- main

```

This configuration uses the Zola Docker image to build your site and tells GitLab CI/CD to publish the contents of the `public` directory, where Zola generates the static site.

3. **Push Changes**: Commit and push `.gitlab-ci.yml` to your GitLab repository. GitLab CI/CD will automatically detect the file and run the pipeline to build and deploy your site.

4. **Check GitLab Pages**: Once the pipeline completes, your site should be live on GitLab Pages. You can find the URL in your project's settings under Pages.

This setup streamlines your deployment process, making it easy to update your website with every git push.





## License
To be defined.

