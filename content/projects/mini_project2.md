+++
title = "IDS721 - Mini Project 2"
description = "Very Fast and Easy Rust AWS Lambda Deployment with Cargo Lambda"
weight = 1

[extra]
# You can also crop the image in the url by adjusting w=/h=
remote_image = "https://gitlab.com/YangOuyang/personal_webpage/-/raw/main/images/AWSLambda_FullAccess.png"
+++
# Mini Project2 Cargo Lambda

## Live Demo

> [Live Demo](https://8vkxqhnk9b.execute-api.us-east-1.amazonaws.com/prod/tendency_calculator?numbers=123,31,25,98,456,2024,24,24) Could be customized with your own numbers by changing the query string after the `numbers` parameter.

## Functionality

Given a sequence of numbers, the function calculates the mean, median, and mode of the sequence.

## Install Cargo Lambda

You can use Homebrew to install Cargo Lambda on macOS and Linux. Run the following commands on your terminal to add our tap, and install it:

```sh
brew tap cargo-lambda/cargo-lambda
brew install cargo-lambda
```

## Create a new Cargo Lambda project

The new subcommand will help you create a new project with a default template. When that's done, change into the new directory:

```sh
cargo lambda new new-lambda-project \
&& cd new-lambda-project
```

## Serve the function locally for testing

Run the Lambda emulator built in with the watch subcommand:

```sh
cargo lambda watch
```

## Generating AWS User Credentials

At this point, you are probably champing at the bit to deploy your Rust function into the AWS Lambda service. In order to do that, we will need to generate some static AWS user credentials, so that we can publish the Lambda function from our local development system.

1. Open the AWS IAM web management console
2. Create a new IAM user
3. Choose Attach Policies Directly option
4. Search for the AWSLambda_FullAccess policy and check it
   ![AWSLambda_FullAccess.png](images%2FAWSLambda_FullAccess.png)
5. Finish the user creation wizard, and open the user details
6. Open the Security Credentials tab
7. Under Access Keys, select the Create Access Key button
8. Choose the Other option, and skip the Description field
9. Copy the Access Key and Secret Access Key values


## Deploy the function to AWS Lambda

```sh
cargo lambda build --release # release build first
cargo lambda deploy
```

### Debugging

I encountered the following error when deploying the function to AWS Lambda:

```sh
➜ cargo lambda deploy                                                                          
Error:   × failed to create function role
├─▶ service error
├─▶ unhandled error
├─▶ unhandled error
╰─▶ Error { code: "AccessDenied", message: "User: arn:aws:iam::490253458749:user/cargo_lambda is not authorized to perform: iam:CreateRole on resource: arn:aws:iam::490253458749:role/cargo-lambda-role-7dd69bf7-b212-43d1-
a094-9401b80d8b77 because no identity-based policy allows the iam:CreateRole action", aws_request_id: "d1ea7525-c92a-4117-8fdb-910b7af10010" }
```

**Note:** grant the user the necessary permissions to create the role.


## Add AWS API Gateway trigger to the function

1. Open the AWS Lambda web management console
2. Click on the function you want to add the trigger to
3. Click on the Add trigger button
4. Choose the API Gateway option
5. Choose the Create a new API option
6. **Choose the REST API option (!important), if you choose the HTTP API option, you will not be able to use the query string parameters**
7. Click on the Add button
8. Access the function using the URL provided in the API Gateway trigger details

## License

For open source projects, say how it is licensed.


