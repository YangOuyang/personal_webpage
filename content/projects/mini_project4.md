+++
title = "IDS721 - Mini Project 4"
description = "Very Fast and Easy Rust AWS Lambda Deployment with Cargo Lambda"
weight = 1

[extra]
# You can also crop the image in the url by adjusting w=/h=
remote_image = "https://gitlab.com/YangOuyang/personal_webpage/-/raw/main/images/mini_project4.png"
+++
# Mini Project4 Actix Webapp

## Initial Setup

1. create a new repository on gitlab and clone it to this local machine
2. `cargo init` to create the basic structure of the project
3. add the following dependencies to the `Cargo.toml` file

```toml
[dependencies]
actix-web = "4"
```

## Create the simple business logic for the web app

1. edit the `main.rs` file according to [Actix Getting Started](https://actix.rs/docs/getting-started) to create a simple web server
2. `cargo run` to start the web server
3. open a web browser and go to `http://localhost:8080/` to see the web server in action
   ![alt text](images/test.png)
4. `Ctrl+C` to stop the web server


## Upgrade the business logic for the web app

1. create a new file `lib.rs` in the `src` directory
2. add the greedy coin algorithm to the `lib.rs` file
3. using `cargo test` to test until the greedy coin algorithm perform correctly
   ![alt text](images/cargo_test.png)
4. edit the `main.rs` file to use the greedy coin algorithm
5. `cargo run` to start the web server and test the greedy coin algorithm
   - <img src="images/business_logic.png" alt="drawing" style="width:200px;"/>


## Containerize the web app with Distroless image

1. create a new file `Dockerfile` in the root directory
2. build the rust project with the following command
   `cargo build --release`
3. add the following content to the `Dockerfile`

```Dockerfile
# Build stage
FROM rust:1.68 AS build
WORKDIR /app
COPY . .
RUN cargo build --release

# Production stage
FROM gcr.io/distroless/cc-debian11
COPY --from=build /app/target/release/mini_project4_actix_webapp /app/

# use non-root user
USER nonroot:nonroot

# Set up App directory
ENV APP_HOME=/app
WORKDIR $APP_HOME

# Expose the port
EXPOSE 8080

# Run the web service on container startup.
ENTRYPOINT [ "/app/mini_project4_actix_webapp" ]
```

3. build the docker image with the following command

```bash
docker build -t actix-webapp .
```

> It will take a while to build the image

- ![alt text](images/image_build_process.png)

- ![alt text](images/actix_webapp_image_created.png)

4. run the docker container with the following command (Make sure the server port of the source code is 0.0.0.0:8080 not 127.0.0.1:8080)

```bash
docker run -p 8080:8080 actix-webapp
```

- ![alt text](images/container_start.png)

5. open a web browser and go to `http://localhost:8080/` to see the web server in action.

- <img src="images/docker_run.png" alt="drawing" style="width:200px;"/> 
- <img src="images/docker_run2.png" alt="drawing" style="width:200px;"/>


6. `docker ps` to see the running container
7. `docker stop` and `docker rm` to stop and remove the running container
