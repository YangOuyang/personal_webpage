+++
title = "Risk Game"
description = "Developed a network game which enables users to attack territories and obtain resources, move and upgrade soldiers, chat and ally with other players."
weight = 1

[extra]
remote_image = "https://gitlab.com/YangOuyang/personal_webpage/-/raw/main/images/risk.jpeg"
link_to = "https://gitlab.oit.duke.edu/risc_group1/ece651-sp23-risc"
+++

